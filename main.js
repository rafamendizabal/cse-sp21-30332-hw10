var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;
var para = document.createElement("p");
var newline = document.createElement("br");
var population = document.createElement("p");
population.setAttribute("id", "population-paragraph");
para.setAttribute("id", "submit-paragraph");

var label_text = document.createTextNode("State info (from first API):");
var population_text = document.createTextNode("Population (from second API): ");

para.appendChild(label_text);
population.appendChild(population_text);

document.body.appendChild(para);
document.body.appendChild(newline);
document.body.appendChild(population);

function getFormInfo(){
    var tl1 = document.getElementById("submit-paragraph");
	tl1.innerHTML = "State info (from first API): ";
    var tl2 = document.getElementById("population-paragraph");
	tl2.innerHTML = "Population (from second API): ";
    var capitalID = document.getElementById("capital-id").value;
    makeNetworkCallToCapitalApi(capitalID);
}

function makeNetworkCallToCapitalApi(capitalID){
    var url = 'http://student13.cse.nd.edu:51022/capitals/' + capitalID;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true); 
    xhr.onload = function(e){
        var response_json = JSON.parse(xhr.responseText);
        var capital = document.createTextNode(response_json['capital']);
        var state = document.createTextNode(", " + response_json['state']);

        para.appendChild(capital);
        para.appendChild(state);

        makeNetworkCallToPopulationsApi(capitalID);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function makeNetworkCallToPopulationsApi(capitalID){
    var url = "https://datausa.io/api/data?drilldowns=State&measures=Population&year=latest";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        var response_json = JSON.parse(xhr.responseText);
        var pop = document.createTextNode(response_json['data'][capitalID]['Year'] + ": " + response_json['data'][capitalID]['Population']);
        
        population.appendChild(pop);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}
